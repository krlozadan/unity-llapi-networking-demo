﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using UnityEngine.AI;
using LLAPI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(NetPlayer))]
public class NavMover : MonoBehaviour, IMovable
{
    [SerializeField]    
    private UEvent_LLClientV3 setDestinationEvent;

    private NetPlayer player;
    private NavMeshAgent agent;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GetComponent<NetPlayer>();
    }
    
    public void SetDestination(Vector3 position)
    {
        agent.SetDestination(position);
        if(player.isOwner)
        {
            setDestinationEvent.Invoke(player.client, position);
        }
    }
}

[System.Serializable]
public class UEvent_LLClientV3 : UnityEvent<LLClient, Vector3> {}