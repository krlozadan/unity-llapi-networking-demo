﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class VisualModel : MonoBehaviour {
    [SerializeField]
    private Transform model;

    [SerializeField]
    private float smoothness;

    private void Update() {
        transform.position = transform.position.Hermite(model.position, smoothness);
    }
}