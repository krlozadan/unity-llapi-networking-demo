﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

namespace LLAPI
{
    public enum NetMessageType : byte
    {
        CONNECTION_ACK,
        DISCONNECT_USER,
        USER_INFO,
        CHAT_WHISPER,   
        CHAT_BROADCAST,
        SPAWN_PLAYER,
        SET_DESTINATION,
        SYNC_PHYSICS
    }
}
