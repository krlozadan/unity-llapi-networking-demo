﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace LLAPI
{
    public class LLServer : MonoBehaviour
    {
        #region Globals

            public UEvent_IntInt UserConnectedEvent;
            public UEvent_IntInt UserDisconnectedEvent;

            [SerializeField]
            private int serverPort = 27000;
            
            [SerializeField]
            private int bufferSize = 1024;
            
            [SerializeField]
            private byte threadPoolSize = 4;

            [SerializeField]
            private SO_NetMessageContainer Messages;

            private int socketID = 0;
            public byte reliableChannel = 0;
            public byte unreliableChannel = 0;

            public bool isReady { get; private set; }

            public Dictionary<int, NetUser> netUsers = new Dictionary<int, NetUser>(); // this holds all users

        #endregion

        #region Life Cycle

            void Start ()
            {
                StartServer();
            }

            private void OnDisable()
            {
                isReady = false;
            }

        #endregion

        #region Server Logic

            public void StartServer()
            {
                Messages.MapMessages();
                
                GlobalConfig gConf = new GlobalConfig
                {
                    ThreadPoolSize = threadPoolSize
                };

                NetworkTransport.Init(gConf);        

                ConnectionConfig config = new ConnectionConfig()
                {
                    SendDelay = 0,
                    MinUpdateTimeout = 1
                };

                reliableChannel = config.AddChannel(QosType.Reliable);
                unreliableChannel = config.AddChannel(QosType.Unreliable);

                HostTopology topology = new HostTopology(config, 16);
                socketID = NetworkTransport.AddHost(topology, serverPort);

                StartCoroutine(Receiver());
                isReady = true;
                Debug.LogFormat("{0}, {1}, {2}", socketID, reliableChannel, unreliableChannel);
            }

            private IEnumerator Receiver()
            {
                int recSocketID, recConnectionID, recChannelID, recDataSize;
                byte error;
                byte[] recBuffer = new byte[bufferSize];

                while(true)
                {
                    NetworkEventType NEType = NetworkTransport.Receive(
                        out recSocketID, 
                        out recConnectionID, 
                        out recChannelID, 
                        recBuffer, 
                        bufferSize, 
                        out recDataSize, 
                        out error
                    );
                    
                    switch(NEType)
                    {
                        case NetworkEventType.Nothing:
                        {
                            yield return null;
                            break;
                        }
                        case NetworkEventType.DataEvent:
                        {
                            OnDataEvent(recConnectionID, recChannelID, recBuffer, recDataSize);
                            break;
                        }
                        case NetworkEventType.ConnectEvent:
                        {
                            OnConnectEvent(recSocketID, recConnectionID);
                            break;
                        }
                        case NetworkEventType.DisconnectEvent:
                        {
                            OnDisconnectEvent(recSocketID, recConnectionID);
                            break;
                        }
                        default:
                        {
                            print("LLServer.Receiver -> " + NEType.ToString());
                            break;
                        }
                    }

                }

            }

            public void OnDataEvent(int connectionID, int channelID, byte[] data, int dataSize)
            {
                Byterizer tmpData = new Byterizer();
                tmpData.LoadDeep(data, dataSize);
                NetMessageType msgType = (NetMessageType)tmpData.PopByte();

                Messages.NetMessagesMap[msgType].Server_ReceiveMessage(connectionID, tmpData, this);
            }

            public void OnConnectEvent(int sockedID, int connectionID)
            {
                NetUser tmp = new NetUser();
                tmp.connectionID = connectionID;
                if(netUsers.ContainsKey(connectionID))
                {
                    // User reconecting
                    Debug.LogFormat("User {0} - {1} reconnected",connectionID, netUsers[connectionID].username);
                }
                else
                {
                    // New user
                    netUsers[connectionID] = tmp;
                    
                    Byterizer b = new Byterizer();
                    b.Push((byte)NetMessageType.CONNECTION_ACK);
                    b.Push(connectionID);

                    SendNetMessage(connectionID, reliableChannel, b.GetBuffer());
                    Debug.LogFormat("User {0} connected", connectionID);
                }
                UserConnectedEvent.Invoke(sockedID, connectionID);
            }

            public void OnDisconnectEvent(int sockedID, int connectionID)
            {
                if(netUsers.ContainsKey(connectionID))
                {
                    netUsers.Remove(connectionID);
                }
                
                Byterizer b = new Byterizer();
                b.Push((byte)NetMessageType.DISCONNECT_USER);
                b.Push(connectionID);

                BroadcastNetMessage(reliableChannel, b.GetBuffer(), connectionID);
                Debug.LogFormat("User {0} disconected", connectionID);
                UserDisconnectedEvent.Invoke(sockedID, connectionID);
            }

            public void SendNetMessage(int conId, int channel, byte[] msgToSend)
            {
                byte err = 0;
                NetworkTransport.Send(socketID, conId, channel, msgToSend, msgToSend.Length, out err);
                if(err != 0)
                {
                    Debug.LogFormat("Error sending message {0}", err);
                }
            }

            public void BroadcastNetMessage(int channel, byte[] msgToSend, int excludeID = -1)
            {
                foreach (var user in netUsers)
                {
                    if(user.Key != excludeID)
                    {
                        SendNetMessage(user.Key, channel, msgToSend);
                    }
                }
            }

            public void MulticastNetmessage(int[] ids, int channel, byte[] msgToSend)
            {
                foreach (int id in ids)
                {
                    SendNetMessage(id, channel, msgToSend);
                }
            }

            public void Kickuser(int userToKick)
            {
                byte err;
                NetworkTransport.Disconnect(socketID, userToKick, out err);
            }

        #endregion

        [System.Serializable]
        public class UEvent_IntInt : UnityEvent<int, int> {}

        #region ONGUI


            private void OnGUI()
            {
                GUILayout.BeginHorizontal();
                    GUILayout.BeginVertical();
                        GUILayout.Label("Users Connected");
                        GUILayout.Space(16);
                        foreach(var user in netUsers)
                        {
                            if (GUILayout.Button(user.Value.username + " - " + user.Value.teamNumber))
                            {
                                Kickuser(user.Key);
                            }
                        }
                    GUILayout.EndVertical();
                    GUILayout.BeginVertical();
                        GUILayout.Label("Messages");
                        GUILayout.Space(16);
                        // TODO: Display Messages
                    GUILayout.EndVertical();
                GUILayout.EndHorizontal();            
            }

        #endregion
    }

}