﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using System.Collections.Generic;
using UnityEngine;

namespace LLAPI
{

    [CreateAssetMenu(menuName = "LLAPI/NetMessageContainer", fileName = "NetMessageContainer")]
    public class SO_NetMessageContainer : ScriptableObject
    {
        [SerializeField]
        private SO_ANetMessage[] NetMessages;

        public Dictionary<NetMessageType, SO_ANetMessage> NetMessagesMap { get; private set; }

        public void MapMessages()
        {
            NetMessagesMap = new Dictionary<NetMessageType, SO_ANetMessage>();
            foreach(var item in NetMessages)
            {
                NetMessagesMap[item.messageType] = item;
            }
        }
    }
}