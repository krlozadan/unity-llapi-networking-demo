﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/SetDestination", fileName = "Message_SetDestination")]
public class Message_SetDestination : SO_ANetMessage
{

    public void OnSetDestination(LLClient client, Vector3 position)
    {
        Byterizer b = new Byterizer();
        b.Push((byte)NetMessageType.SET_DESTINATION);
        b.Push(position);

        client.SendNetMessage(client.reliableChannel, b.GetBuffer());
    }

    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        Vector3 position = data.PopVector3();
        int conId = data.PopInt32();
        
        NetPlayer.playerList[conId].mover.SetDestination(position);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {
        Vector3 position = data.PopVector3();
        data.Push(clientConnectionId);

        server.BroadcastNetMessage(server.reliableChannel, data.GetBuffer(), clientConnectionId);

        NetPlayer.playerList[clientConnectionId].mover.SetDestination(position);
    }

}