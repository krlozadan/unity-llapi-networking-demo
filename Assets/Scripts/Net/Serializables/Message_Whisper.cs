﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/Whisper", fileName = "Message_Whisper")]
public class Message_Whisper : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
		int conID = data.PopInt32();
		string msg = data.PopString();

		client.AddMessageToQ(client.netUsers[conID].username + " Whisper: " + msg);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {
        int destinationID = data.PopInt32();
        string chatMsg = data.PopString();

        Byterizer b = new Byterizer();
        b.Push((byte)NetMessageType.CHAT_WHISPER);
        b.Push(clientConnectionId);
        b.Push(chatMsg);

        server.SendNetMessage(destinationID, server.reliableChannel, b.GetBuffer());
    }

}