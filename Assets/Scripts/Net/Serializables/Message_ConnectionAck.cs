﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/ConnectionAck", fileName = "Message_ConnectionAck")]
public class Message_ConnectionAck : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        int conID = data.PopInt32();
        NetUser tmp = new NetUser();
        tmp.connectionID = conID;
        tmp.username = client.username;
        tmp.teamNumber = client.teamNumber;
        client.netUsers[conID] = tmp;
        client.netID = conID;

        SendServerMyUserInfo(conID, client);
        SendServerSpawnMyPlayer(client);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {

    }

    private void SendServerMyUserInfo(int conID, LLClient client)
    {
        Byterizer b = new Byterizer();
        b.Push((byte)NetMessageType.USER_INFO);
        b.Push(client.username);
        b.Push(client.teamNumber);
        client.SendNetMessage(client.reliableChannel, b.GetBuffer());
        Debug.Log("I'm registered @ " + conID +" as "+ client.username);
    }

    private void SendServerSpawnMyPlayer(LLClient client)
    {
        Transform spawnPoint = GameObject.FindGameObjectWithTag("Spawn").transform;
        Byterizer b = new Byterizer();
        b.Push((byte)NetMessageType.SPAWN_PLAYER);
        b.Push("NetPhysicsPlayer");
        b.Push(spawnPoint.position);
        client.SendNetMessage(client.reliableChannel, b.GetBuffer());
    }

}