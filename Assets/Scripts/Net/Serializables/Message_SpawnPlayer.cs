﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/SpawnPlayer", fileName = "Message_SpawnPlayer")]
public class Message_SpawnPlayer : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        string prefabName = data.PopString();
        Vector3 position = data.PopVector3();
        int userId = data.PopInt32();
        var nUsr = client.netUsers[userId];

        SpawnPlayer(prefabName, position, nUsr, client, userId == client.netID);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {   
        var nUser = server.netUsers[clientConnectionId];
        string prefabName = data.PopString();
        Vector3 position = data.PopVector3();

        SpawnPlayer(prefabName, position, nUser, null, false);
        data.Push(clientConnectionId);
        server.BroadcastNetMessage(server.reliableChannel, data.GetBuffer());
    }

    private void SpawnPlayer(string prefabName, Vector3 position, NetUser user, LLClient client, bool isOwner)
    {
        var prefab = Resources.Load<NetPhysicsPlayer>(prefabName);
        var clone = Instantiate(prefab, position, Quaternion.identity);
        clone.NetInitialize(user.connectionID, user.teamNumber, client, isOwner);
        
        if(isOwner)
        {
            clone.gameObject.AddComponent<PhysicsPlayerController>();
        }
    }
}