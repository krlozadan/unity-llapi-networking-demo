﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;
using System;

[CreateAssetMenu(menuName = "LLAPI/Messages/SyncPhysics", fileName = "Message_SyncPhysics")]
public class Message_SyncPhysics : SO_ANetMessage
{

    public void OnSendPhysicsUpdate(LLClient client, Vector3 position, Vector3 velocity)
    {
        Byterizer b = new Byterizer();
        b.Push((byte)NetMessageType.SYNC_PHYSICS);
        b.Push(DateTime.UtcNow);
        b.Push(position);
        b.Push(velocity);

        client.SendNetMessage(client.unreliableChannel, b.GetBuffer());
    }

    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        DateTime timestamp = data.PopDatetime();
        Vector3 position = data.PopVector3();
        Vector3 velocity = data.PopVector3();
        int conId = data.PopInt32();

        if(!NetPhysicsPlayer.playerList.ContainsKey(conId)) return;
        
        NetPhysicsPlayer.playerList[conId].syncer.UpdateRigidBody(timestamp, position, velocity);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {
        DateTime timestamp = data.PopDatetime();
        Vector3 position = data.PopVector3();
        Vector3 velocity = data.PopVector3();
        data.Push(clientConnectionId);

        server.BroadcastNetMessage(server.unreliableChannel, data.GetBuffer(), clientConnectionId);

        NetPhysicsPlayer.playerList[clientConnectionId].syncer.UpdateRigidBody(timestamp, position, velocity);
    }
}