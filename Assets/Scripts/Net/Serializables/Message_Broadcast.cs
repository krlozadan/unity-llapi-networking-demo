﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/Broadcast", fileName = "Message_Broadcast")]
public class Message_Broadcast : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        int conID = data.PopInt32();
        string msg = data.PopString();
        client.AddMessageToQ("ALL: " + msg);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {
        string chatMsg = "Hello from " + data.PopString();
        Byterizer b = new Byterizer();

        b.Push((byte)NetMessageType.CHAT_BROADCAST);
        b.Push(clientConnectionId);
        b.Push(chatMsg);
        
        server.BroadcastNetMessage(server.reliableChannel, b.GetBuffer());
    }

}