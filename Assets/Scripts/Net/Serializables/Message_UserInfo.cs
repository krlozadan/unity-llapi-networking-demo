﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/UserInfo", fileName = "Message_UserInfo")]
public class Message_UserInfo : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        NetUser tmp = new NetUser();
        tmp.connectionID = data.PopInt32();
        tmp.username = data.PopString();
        tmp.teamNumber = data.PopInt32();
        client.netUsers[tmp.connectionID] = tmp;
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {
        string username = data.PopString();
        int teamNumber = data.PopInt32();

        server.netUsers[clientConnectionId].username = username;
        server.netUsers[clientConnectionId].teamNumber = teamNumber;

        // SpawnNetPlayerServer(clientConnectionId, teamNumber);

        Byterizer b = new Byterizer();
        b.Push((byte)NetMessageType.USER_INFO);
        b.Push(clientConnectionId);
        b.Push(username);
        b.Push(teamNumber);

        server.BroadcastNetMessage(server.reliableChannel, b.GetBuffer(), clientConnectionId);

        foreach(var user in server.netUsers)
        {
            if(user.Key != clientConnectionId)
            {
                Byterizer b2 = new Byterizer();
                b2.Push((byte)NetMessageType.USER_INFO);
                b2.Push(user.Value.connectionID);
                b2.Push(user.Value.username);
                b2.Push(user.Value.teamNumber);
                server.SendNetMessage(clientConnectionId, server.reliableChannel, b2.GetBuffer());
            }
        }

        SendSpawnExistingPlayerToNewUser(clientConnectionId, server);
    }

    private void SendSpawnExistingPlayerToNewUser(int clientConnectionId, LLServer server)
    {
        foreach(var item in NetPhysicsPlayer.playerList)
        {
            if(item.Key != clientConnectionId)
            {
                Byterizer b = new Byterizer();
                b.Push((byte)NetMessageType.SPAWN_PLAYER);
                b.Push("NetPhysicsPlayer");
                b.Push(item.Value.transform.position);
                b.Push(item.Value.netID);
                server.SendNetMessage(clientConnectionId, server.reliableChannel, b.GetBuffer());
            }
        }
    }
}