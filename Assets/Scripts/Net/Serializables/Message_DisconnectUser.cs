﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/DisconnectUser", fileName = "Message_DisconnectUser")]
public class Message_DisconnectUser : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLClient client)
    {
        int conID = data.PopInt32();
        RemovePlayer(0, conID);
        if(client.netUsers.ContainsKey(conID))
        {
            client.netUsers.Remove(conID);
        }
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server)
    {
        if(server.netUsers.ContainsKey(clientConnectionId))
        {
            server.Kickuser(clientConnectionId);
        }   
    }

    public void RemovePlayer(int sockId, int id)    
    {
        if(NetPhysicsPlayer.playerList.ContainsKey(id))
        {
            Destroy(NetPhysicsPlayer.playerList[id].gameObject);
        }
    }

}