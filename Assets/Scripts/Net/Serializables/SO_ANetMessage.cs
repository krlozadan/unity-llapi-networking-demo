﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

namespace LLAPI
{
    public abstract class SO_ANetMessage : ScriptableObject
    {

        public NetMessageType messageType;

        // public abstract void Server_SendMessage(Byterizer data, LLServer server);
        // public abstract void Client_SendMessage(int clientConnectionId, Byterizer data, LLClient client);
        
        public abstract void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLServer server);
        public abstract void Client_ReceiveMessage(Byterizer data, LLClient client);
    }
}