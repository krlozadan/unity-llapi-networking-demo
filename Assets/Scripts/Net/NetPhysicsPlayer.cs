﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using System.Collections.Generic;
using UnityEngine;
using LLAPI;

public class NetPhysicsPlayer : MonoBehaviour
{
    public static Dictionary<int, NetPhysicsPlayer> playerList = new Dictionary<int, NetPhysicsPlayer>();

    public int netID { get; private set; }
    public int teamNumber { get; private set; }
    public bool isOwner { get; private set; }
    public PhysicsSyncer syncer { get; private set; }
    public LLClient client { get; private set; }

    private bool isReady = false;


    private void Awake()
    {
        syncer = GetComponentInChildren<PhysicsSyncer>();
    }

    public void NetInitialize(int id, int team, LLClient theClient, bool owner)
    {
        netID = id;
        teamNumber = team;
        isOwner = owner;
        client = theClient;
        playerList.Add(netID, this);
        Debug.Log("Net Initialize: " + id + " - " + team);
        isReady = true;
    }

    private void OnDestroy()
    {
        if(isReady && playerList.ContainsKey(netID))
        {
            playerList.Remove(netID);
        }
    }
}