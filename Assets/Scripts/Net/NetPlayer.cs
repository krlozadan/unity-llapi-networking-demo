﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using System.Collections.Generic;
using UnityEngine;
using LLAPI;

[RequireComponent(typeof(IMovable))]
[RequireComponent(typeof(Renderer))]
public class NetPlayer : MonoBehaviour
{
    public static Dictionary<int, NetPlayer> playerList = new Dictionary<int, NetPlayer>();

    public int netID { get; private set; }
    public int teamNumber { get; private set; }
    public IMovable mover { get; private set; }
    public bool isOwner { get; private set; }
    public LLClient client { get; private set; }

    private bool isReady = false;
    
    private Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
        mover = GetComponent<IMovable>();
    }

    public void NetInitialize(int id, int team, LLClient theClient, bool owner)
    {
        netID = id;
        teamNumber = team;
        isOwner = owner;
        client = theClient;
        playerList.Add(netID, this);
        rend.material.color = teamNumber == 0 ? Color.red : teamNumber == 1 ? Color.green : Color.black;
        Debug.Log("Net Initialize: " + id + " - " + team);
        isReady = true;
    }

    private void OnDestroy()
    {
        if(isReady && playerList.ContainsKey(netID))
        {
            playerList.Remove(netID);
        }
    }
}