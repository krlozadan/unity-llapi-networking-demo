﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace LLAPI
{
    public class LLClient : MonoBehaviour
    {
        #region Globals

            [SerializeField]
            private string serverAddress = "127.0.0.1";

            [SerializeField]
            private int serverPort = 27000;
            
            [SerializeField]
            private int bufferSize = 1024;

            [SerializeField]
            private byte threadPoolSize = 4;

            [SerializeField]
            private SO_NetMessageContainer Messages;

            public int socketID { get; private set; }
            public int netID { get; set; }
            public int serverConnectionID { get; private set; }
            public byte reliableChannel { get; private set; }
            public byte unreliableChannel { get; private set; }
            public bool isReady { get; private set; }
            

            [Space]
            [Header("User Details")]
            public string username = "krlozadan";
            
            [Range(0, 1f)]
            public int teamNumber = 0;

            public Dictionary<int, NetUser> netUsers = new Dictionary<int, NetUser>();

        #endregion

        #region Life Cycle

            void Start ()
            {
                ConnectToServer();
            }

            private void OnDisable()
            {
                DisconnectFromServer();
            }

        #endregion

        #region Client Logic

            public void ConnectToServer()
            {
                Messages.MapMessages();
                
                GlobalConfig gConf = new GlobalConfig
                {
                    ThreadPoolSize = threadPoolSize
                };

                NetworkTransport.Init(gConf);        

                ConnectionConfig config = new ConnectionConfig()
                {
                    SendDelay = 0,
                    MinUpdateTimeout = 1
                };

                reliableChannel = config.AddChannel(QosType.Reliable);
                unreliableChannel = config.AddChannel(QosType.Unreliable);

                HostTopology topology = new HostTopology(config, 16);
                socketID = NetworkTransport.AddHost(topology, 0);
                byte error = 0;
                serverConnectionID = NetworkTransport.Connect(socketID, serverAddress, serverPort, 0, out error);
                if(error != 0)
                {
                    Debug.LogFormat("Error connecting to server: {0}", error);
                }

                StartCoroutine(Receiver());
                isReady = true;
                Debug.LogFormat("{0}, {1}, {2}, {3}", socketID, reliableChannel, unreliableChannel, serverConnectionID);
            }

            private IEnumerator Receiver ()
            {
                int recSocketID, recConnectionID, recChannelID, recDataSize;
                byte error;
                byte[] recBuffer = new byte[bufferSize];

                while(true)
                {
                    NetworkEventType NEType = NetworkTransport.Receive(
                        out recSocketID, 
                        out recConnectionID, 
                        out recChannelID, 
                        recBuffer, 
                        bufferSize, 
                        out recDataSize, 
                        out error
                    );
                
                    switch(NEType)
                    {
                        case NetworkEventType.Nothing:
                        {
                            yield return null;
                            break;
                        }
                        case NetworkEventType.DataEvent:
                        {
                            OnDataEvent(recConnectionID, recChannelID, recBuffer, recDataSize);
                            break;
                        }
                        case NetworkEventType.ConnectEvent:
                        {
                            OnConnectEvent(recSocketID, recConnectionID);
                            break;
                        }
                        case NetworkEventType.DisconnectEvent:
                        {
                            OnDisconnectEvent(recSocketID, recConnectionID);
                            break;
                        }
                        default:
                        {
                            Debug.Log("LLClient .Receiver -> " + NEType.ToString(), this);
                            break;
                        }
                    }
                }
            }

            public void OnDataEvent(int connectionID, int channelID, byte[] data, int dataSize)
            {   
                Byterizer tmpData = new Byterizer();
                tmpData.LoadDeep(data, dataSize);
                NetMessageType msgType = (NetMessageType)tmpData.PopByte();

                Messages.NetMessagesMap[msgType].Client_ReceiveMessage(tmpData, this);
            }

            public void OnConnectEvent(int sockedID, int connectionID)
            {
                Debug.LogFormat("User {0} conected at socket {1}", connectionID, sockedID);
            }

            public void OnDisconnectEvent(int sockedID, int connectionID)
            {
                Debug.LogFormat("User {0} disconected from socket {1}", connectionID, sockedID);
            }

            public void SendNetMessage(int channel, byte[] msgToSend)
            {
                byte err = 0;
                NetworkTransport.Send(socketID, serverConnectionID, channel, msgToSend, msgToSend.Length, out err);
                if(err != 0)
                {
                    Debug.LogFormat("Error sending message {0}", err);
                }
            }

            public void DisconnectFromServer()
            {
                isReady = false;
                byte err;
                NetworkTransport.Disconnect(socketID, serverConnectionID, out err);
            }

        #endregion

        #region ONGUI

            private Queue<string> uiMessages = new Queue<string>();

            public void AddMessageToQ(string msg)
            {
                if(uiMessages.Count >= 20)
                {
                    uiMessages.Dequeue();
                }
                uiMessages.Enqueue(msg);
            }

            private void OnGUI()
            {
                GUILayout.BeginHorizontal();
                    GUILayout.BeginVertical();
                        GUILayout.Label("Users Connected");
                        GUILayout.Space(16);
                        if(GUILayout.Button("Broadcast Hello"))
                        {
                            Byterizer b = new Byterizer();
                            b.Push((byte)NetMessageType.CHAT_BROADCAST);
                            b.Push("Hello From " + username);
                            SendNetMessage(reliableChannel, b.GetBuffer());
                        }
                        GUILayout.Space(16);
                        foreach(var user in netUsers)
                        {
                            if (GUILayout.Button(user.Value.username + " - " + user.Value.teamNumber))
                            {
                                Byterizer b = new Byterizer();
                                b.Push((byte)NetMessageType.CHAT_WHISPER);
                                b.Push(user.Value.connectionID);
                                b.Push("Hello From " + username);
                                SendNetMessage(reliableChannel, b.GetBuffer());
                            }
                        }
                    GUILayout.EndVertical();
                    GUILayout.BeginVertical();
                        GUILayout.Label("Messages");
                        GUILayout.Space(16);
                        foreach(string msg in uiMessages)
                        {
                            GUILayout.Label(msg);
                        }
                    GUILayout.EndVertical();
                GUILayout.EndHorizontal();            
            }

        #endregion
    }
}