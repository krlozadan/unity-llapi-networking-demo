﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

[RequireComponent(typeof(IMovable))]
public class PlayerController : MonoBehaviour
{
    private Camera cam;
    private IMovable mover;

    private void Awake()
    {
        mover = GetComponent<IMovable>();
        cam = Camera.main;
    }

    private void Update()
    {
        if(cam == null) return;

        if(Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                mover.SetDestination(hit.point);
            }
        }
    }
}