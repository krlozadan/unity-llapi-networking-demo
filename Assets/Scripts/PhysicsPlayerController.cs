﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class PhysicsPlayerController : MonoBehaviour
{

    [SerializeField]
    private float moveSpeed = 20f;
    [SerializeField]
    private float jumpForce = 8f;
    
    // Properties
    private float xInput;
    private float zInput;
    private bool jumpPressed;

    // Components
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponentInChildren<Rigidbody>();
    }

    private void Update()
    {
        UpdateInputState();
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(xInput, 0f, zInput) * moveSpeed * Time.fixedDeltaTime, ForceMode.VelocityChange);
        if(jumpPressed) 
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
        }
        jumpPressed = false;
    }

    private void UpdateInputState()
    {
        xInput = Input.GetAxisRaw("Horizontal");
        zInput = Input.GetAxisRaw("Vertical");
        jumpPressed = Input.GetKeyDown(KeyCode.Space);
    }
}