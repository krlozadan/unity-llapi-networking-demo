﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class ExtansionExample : MonoBehaviour
{

    [Range(0f, 1f)]
    public float factor = 0.5f;

    Vector3 v3 = Vector3.zero;

    public Transform tr;
    
    private void Update()
    {
        tr.position = Vector3.zero.Hermite(Vector3.one, factor);
        print(v3.Hermite(Vector3.up, 0.1f));
    }
}