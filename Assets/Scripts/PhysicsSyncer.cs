﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using LLAPI;
using UnityEngine.Events;
using System;

public class PhysicsSyncer : MonoBehaviour
{
    [SerializeField]    
    private UEvent_LLClientPhysicsUpdate sendPhysicsUpdateEvent;
    
    private NetPhysicsPlayer player;
    private Rigidbody rb;

    private void Awake()
    {
        player = GetComponentInParent<NetPhysicsPlayer>();
        rb = GetComponent<Rigidbody>();
    }

    public void UpdateRigidBody(DateTime timestamp, Vector3 position, Vector3 velocity) 
    {
        TimeSpan deltaTime = DateTime.UtcNow - timestamp;
        float latency = (float)deltaTime.TotalSeconds;
        Vector3 newPos = position + velocity * latency;
        rb.MovePosition(newPos);
    }
    
    private void FixedUpdate()
    {
        if(player.isOwner)
        {
            sendPhysicsUpdateEvent.Invoke(player.client, rb.position, rb.velocity);
        }
    }
}

[System.Serializable]
public class UEvent_LLClientPhysicsUpdate : UnityEvent<LLClient, Vector3, Vector3> {}
